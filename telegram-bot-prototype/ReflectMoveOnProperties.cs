﻿using System;
using System.Linq;

namespace BotOnStateMachine
{
    public class Reflect
    {
        public static object Move(object value, params string[] properties)
        {
            var currentValue = value;
            foreach (var property in properties)
            {
                if (currentValue == null)
                    return null;
                var prop = currentValue.GetType().GetProperties().FirstOrDefault(x => x.Name == property) ??
                           throw new Exception("Такого свойства не существует");
                currentValue = prop.GetValue(currentValue);
            }


            return currentValue;
        }
    }

    public static class ReflectExtentions
    {
        public static bool IsNull(this object t) => t == null;
        public static bool IsNotNull(this object t) => t != null;
    }
    
    
}