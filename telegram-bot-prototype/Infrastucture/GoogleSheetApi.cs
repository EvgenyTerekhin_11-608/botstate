using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using Microsoft.Extensions.Configuration;

namespace BotOnStateMachine.Infrastucture
{
    public class GoogleSheetApi
    {
        #region creds

        public string SpreadsheetId { get; set; } = "1jUTlFM6o-LHOBHs47zraNo3gYbP1-EjJ99AwsHlPbCY";
        public UserCredential credential;
        const string credPath = "token.json";
        static string[] Scopes = {SheetsService.Scope.SpreadsheetsReadonly};
        static string ApplicationName = "Google Sheets API .NET Quickstart";

        #endregion

        private SheetsService SheetService { get; set; }


        public GoogleSheetApi()
        {
            #region connection

            using (var stream =
                new FileStream(
                    @"C:\Users\evgeniy\Documents\work\telegram-bot-tests\bin\Debug\netcoreapp2.1\credentials.json",
                    FileMode.Open, FileAccess.Read))
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            SheetService = new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName
            });

            #endregion
        }


        public IList<IList<object>> GetListInfo(string listName)
        {
            var range = $"{listName}";
            var request =
                SheetService.Spreadsheets.Values.Get(SpreadsheetId, range).Execute();
            return request.Values;
        }

       
        public IList<IList<object>> GetAreaInfo(string listname, string left, string rigth)
        {
            var range = $"{listname}!{left}:{rigth}";
            var request =
                SheetService.Spreadsheets.Values.Get(SpreadsheetId, range).Execute();
            return request.Values;
        }
    }
}