using BotOnStateMachine.ARef.Infrastructure;

namespace BotOnStateMachine
{
    public static class DataSetExtenstions
    {
        public static DeviationCard GetDeviationCard(this DataSet dataSet)
        {
            return dataSet.ActionServiceSet<DeviationCard>();
        }

        public static Function SelectedFunction(this DataSet dataSet)
        {
            return GetDeviationCard(dataSet).FunctionDto;
        }

        public static string WrotenText(this DataSet dataSet)
        {
            return GetDeviationCard(dataSet).Text;
        }


        public static Employee SelectedEmployee(this DataSet dataSet)
        {
            return GetDeviationCard(dataSet).EmployeeInf;
        }
    }
}