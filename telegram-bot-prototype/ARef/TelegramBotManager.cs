﻿using System.Net;
using BotOnStateMachine.ARef;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using BotOnStateMachine.Infrastucture;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;

namespace BotOnStateMachine
{
    public class TelegramBotManager
    {
        private static TelegramBotClient _client;

        private State CurrentState;
        private InfoStorage InfoStorage { get; set; }

        private SpeedSheetApi sheetApi { get; set; }

        private UsersInformation UsersInformation { get; set; }
        private Builder Builder { get; set; }

        private Collector Collector { get; set; }

        public TelegramBotManager()
        {
            var token = "644453853:AAGbY9hjcnKbQ-_qjdz-WkbKk5DCS5lC8jM";
            var wp = new WebProxy("185.17.123.46:65233");
            wp.Credentials = new NetworkCredential("pesoshin", "L0a8ReO");
            _client = new TelegramBotClient(token, wp);
            Collector = new Collector(_client);
            _client.OnUpdate += onUpdate;
            _client.StartReceiving();
            while (true)
            {
            }
        }

        private void onUpdate(object sender, UpdateEventArgs e)
        {
            var chatId = e.Update.Message?.From?.Id ?? e.Update.CallbackQuery.From.Id;
            var username = e.Update.Message?.From?.Username ?? e.Update.CallbackQuery.From.Username;
            var state = Collector.ReCreateEntities(chatId, username);
            new MessageHandler(state, e.Update).Handle();
        }
    }

    public class MessageHandler
    {
        private readonly State State;
        private readonly Update Update;

        public MessageHandler(State state, Update update)
        {
            State = state;
            Update = update;
        }

        public void Handle()
        {
            State.Execute(Update);
        }
    }
}