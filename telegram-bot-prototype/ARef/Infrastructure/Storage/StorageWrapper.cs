using System.Collections;
using System.Collections.Generic;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.ARef.Infrastructure.Storage;

namespace BotOnStateMachine.ARef.InfrastructureStorageWrapper
{
    public class StorageWrapper<TStorage> where TStorage : Storage
    {
        protected readonly IUserService _userService;
        protected TStorage _storage { get; }

        public StorageWrapper(TStorage storage, IUserService userService)
        {
            _userService = userService;
            _storage = storage;
        }

        public T Set<T>()
        {
            return _storage.Set<TStorage, T>();
        }

        public T Set<T>(string name) where T : class
        {
            return _storage.Set<TStorage, T>(name);
        }
    }

    public interface IImutableValueWrapper
    {
        IEnumerable<T> SetCollection<T>() where T : class;
    }

    public interface IMutableValueWrapper
    {
        T Set<T>() where T : class;
        T Set<T>(string name) where T : class;
        List<T> SetCollection<T>() where T : class;
    }
}