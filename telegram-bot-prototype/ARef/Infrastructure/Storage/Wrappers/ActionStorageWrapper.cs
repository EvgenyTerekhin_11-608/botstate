using System.Collections;
using System.Collections.Generic;
using BotOnStateMachine.ARef.Infrastructure.Storage;
using BotOnStateMachine.ARef.InfrastructureStorageWrapper;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class ActionStorageWrapper : StorageWrapper<UserActionInfoStorage>, IMutableValueWrapper
    {
        public ActionStorageWrapper(UserActionInfoStorage storage, IUserService userService) : base(storage,
            userService)
        {
        }

        public T Set<T>() where T : class
        {
            return _storage.Set<UserActionInfoStorage, T>();
        }

        public T Set<T>(string name)
            where T : class
        {
            return _storage.Set<UserActionInfoStorage, T>(name);
        }

        public List<T> SetCollection<T>() where T : class
        {
            return _storage.Set<UserActionInfoStorage, List<T>>();
        }
    }
}