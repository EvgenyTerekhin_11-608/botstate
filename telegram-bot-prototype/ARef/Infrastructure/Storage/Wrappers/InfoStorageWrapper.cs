using System.Collections;
using System.Collections.Generic;
using BotOnStateMachine.ARef.Infrastructure.Storage;
using BotOnStateMachine.ARef.InfrastructureStorageWrapper;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class InfoStorageWrapper : StorageWrapper<InfoStorage>, IImutableValueWrapper
    {
        public InfoStorageWrapper(InfoStorage storage, IUserService userService) : base(storage, userService)
        {
        }

        public IEnumerable<T> SetCollection<T>() where T : class
        {
            return _storage.ImutableFilter<InfoStorage, T>(_userService);
        }
    }
}