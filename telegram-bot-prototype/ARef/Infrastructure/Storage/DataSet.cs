using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using BotOnStateMachine.ARef.InfrastructureStorageWrapper;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class DataSet
    {
        private readonly InfoStorageWrapper _infoStorage;
        private readonly ActionStorageWrapper _actionService;

        public DataSet(
            InfoStorageWrapper infoStorage,
            ActionStorageWrapper actionService)
        {
            _actionService = actionService;
            _infoStorage = infoStorage;
        }

        public IEnumerable<T> InfoServiceSetCollection<T>()
            where T : class
        {
            return _infoStorage.SetCollection<T>();
        }

        public T ActionServiceSet<T>()
            where T : class
        {
            return _actionService.Set<T>();
        }

        public List<T> ActionServiceSetCollection<T>() where T : class
        {
            var s = _actionService.SetCollection<T>();
            return s;
        }


        public T ActionServiceSet<T>(string name)
            where T : class
        {
            return _actionService.Set<T>(name);
        }
    }


    #region notUse

    //not use for bot
    public class DataSet<TInfoStorage, TActionStorage>
        where TInfoStorage : Storage.Storage
        where TActionStorage : Storage.Storage

    {
        private readonly StorageWrapper<TInfoStorage> _infoStorage;
        private readonly StorageWrapper<TActionStorage> _actionStorage;

        public DataSet(
            StorageWrapper<TInfoStorage> infoStorage,
            StorageWrapper<TActionStorage> actionStorage)
        {
            _actionStorage = actionStorage;
            _infoStorage = infoStorage;
        }

        public T ReadStorageSet<T>()
            where T : class
        {
            return _infoStorage.Set<T>();
        }

        public IEnumerable<T> ReadStorageCollection<T>()
            where T : class

        {
//            return _infoStorage.SetCollection<T>();
            return null;
        }


        public T ReadStorageSet<T>(string name)
            where T : class
        {
            return _infoStorage.Set<T>(name);
        }

        public T WriteStorageSet<T>()
            where T : class
        {
            return _actionStorage.Set<T>();
        }

        public T WriteStorageSet<T>(string name)
            where T : class
        {
            return _actionStorage.Set<T>(name);
        }
    }

    #endregion
}