using System.Collections.Generic;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class InfoStorage : Storage.Storage
    {
        public IEnumerable<Function> Functions { get; set; }
        public IEnumerable<Employee> Employees { get; set; }

        public InfoStorage()
        {
            Functions = new Seed().GetFunction();
            Employees = new Seed().GetEmployee();
        }
    }
}