using System.Collections.Generic;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public abstract class PermissionFilter<TEntity> : IPermissionFilter<TEntity>
    {
        private readonly IUserService UserService;

        public PermissionFilter(IUserService userService)
        {
            UserService = userService;
        }

        public abstract IEnumerable<TEntity> Filter(IEnumerable<TEntity> infoStorage);
    }
}