using System.Collections.Generic;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public interface IPermissionFilter<TEntity>
    {
        IEnumerable<TEntity> Filter(IEnumerable<TEntity> infoStorage);
    }
}