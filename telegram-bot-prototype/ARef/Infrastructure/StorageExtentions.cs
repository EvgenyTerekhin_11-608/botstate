using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using BotOnStateMachine.Infrastucture;

namespace BotOnStateMachine.ARef.Infrastructure.Storage
{
    public static class StorageExtentions
    {
        public static Tkey Set<Tstorage, Tkey>(this Tstorage storage) where Tstorage : Storage
        {
            var allproperties = storage.GetType().GetProperties();
            var properties = storage.GetType().GetProperties()
                .Where(x => x.PropertyType.Name == typeof(Tkey).Name);
            return GetResultByProperty<Tstorage, Tkey>(storage, properties);
        }

        public static IEnumerable<Tkey> SetIEnumerable<Tstorage, Tkey>(this Tstorage storage) where Tstorage : Storage
        {
            var properties = storage.GetType().GetProperties()
                .Where(x => x.PropertyType.IsAssignableFrom(typeof(IEnumerable<Tkey>)));

            return GetResultByProperty<Tstorage, IEnumerable<Tkey>>(storage, properties);
        }

        public static Tkey Set<Tstorage, Tkey>(this Tstorage storage, string name) where Tstorage : Storage
        {
            var properties = storage.GetType().GetProperties()
                .Where(x => x.PropertyType.Name == typeof(Tkey).Name && x.Name == name);
            return GetResultByProperty<Tstorage, Tkey>(storage, properties);
        }

        public static IEnumerable<TKey> ImutableFilter<Tstorage, TKey>(this Tstorage storage, IUserService userService)
            where Tstorage : Storage
        {
            var fitlers = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(x => !x.IsDynamic)
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => x.GetInterfaces().Any(y =>
                    y.IsGenericType && y.GetGenericTypeDefinition()
                    == typeof(IPermissionFilter<>) && y.GenericTypeArguments.FirstOrDefault() == typeof(TKey)))
                .ToArray();

            var activateFilters = fitlers.Select(x =>
                new Builder().Build(x, new object[] {userService})).ToList();
            var enumer = SetIEnumerable<Tstorage, TKey>(storage);

            if (activateFilters.Any())
                return ((IPermissionFilter<TKey>) activateFilters.First()).Filter(enumer);
            else
            {
                return enumer;
            }
        }

        public static Tkey GetResultByProperty<TStorage, Tkey>(TStorage storage, IEnumerable<PropertyInfo> properties)
            where TStorage : Storage
        {
            if (!properties.Any())
                throw new Exception("Значения такого  типа не существует");
            if (properties.Count() > 1)
                throw new Exception(
                    "Существует несколько значений данного типа, воспользуйтесь перегурзкой Set<T>(string name)");
            var property = properties.First();
            var param = Expression.Parameter(storage.GetType());
            var exp_property = Expression.Property(param, property);
            var lambda = Expression.Lambda<Func<TStorage, Tkey>>(exp_property, new ParameterExpression[] {param});
            var func = lambda.Compile();
            var result = func.Invoke(storage);
            return result;
        }
    }
}