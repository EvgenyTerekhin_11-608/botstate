using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class Function
    {
        public readonly Function ParentFunction;
        public string Name { get; set; }

        public Function(string name,Function parentFunction)
        {
            ParentFunction = parentFunction;
            Name = name;
        }
    }

    public class Employee
    {
        public string Name { get; set; }
        public Employee Parent { get; set; }

        public Employee(string Name,Employee parent)
        {
            this.Name = Name;
            Parent = parent;
        }
    }

    public class Seed
    {
        public Seed()
        {
           
        }

        public IEnumerable<Function> GetFunction()
        {
            var list = new List<Function>();
            var functions_level_1 = Enumerable.Range(1, 2).Select(x => new Function($"Функция {x}", null)).ToList();
            var functions_level_2_1 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 1 {x}", functions_level_1[0])).ToList();
            var functions_level_2_2 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 2 {x}", functions_level_1[1])).ToList();
            var functions_level_3_1_1 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 1 1 {x}", functions_level_2_1[0])).ToList();
            var functions_level_3_1_2 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 1 2 {x}", functions_level_2_1[1])).ToList();
            var functions_level_3_2_1 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 2 1 {x}", functions_level_2_2[0]));
            var functions_level_3_2_2 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 2 2 {x}", functions_level_2_2[1]));
            var functions_level_3_3_1 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 3 1 {x}", functions_level_3_1_1[0]));
            var functions_level_3_3_2 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 3 2 {x}", functions_level_3_1_1[1]));
            var functions_level_3_4_1 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 4 1 {x}", functions_level_3_1_2[0]));
            var functions_level_3_4_2 = Enumerable.Range(1, 2).Select(x => new Function($"Функция 4 2 {x}", functions_level_3_1_2[1]));
            list.AddRange(functions_level_1);
            list.AddRange(functions_level_2_1);
            list.AddRange(functions_level_2_2);
            list.AddRange(functions_level_3_1_1);
            list.AddRange(functions_level_3_2_1);
            list.AddRange(functions_level_3_2_2);
            list.AddRange(functions_level_3_3_2);
            list.AddRange(functions_level_3_4_1);
            list.AddRange(functions_level_3_4_2);
            return list;
        }

        public IEnumerable<Employee> GetEmployee()
        {
            var list = new List<Employee>();
            var employee_l_1  =    Enumerable.Range(1, 2).Select(x => new Employee($"Функция {x}", null)).ToList();
            var employee_2_1  =     Enumerable.Range(1, 2).Select(x => new Employee($"Функция 1 {x}", employee_l_1[0])).ToList();
            var employee_l_2_2         = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 2 {x}", employee_l_1[1])).ToList();
            var employee_3_1_1 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 1 1 {x}", employee_2_1[0])).ToList();
            var employee_3_1_2 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 1 2 {x}", employee_2_1[1])).ToList();
            var employee_3_2_1 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 2 1 {x}", employee_l_2_2[0]));
            var employee_3_2_2 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 2 2 {x}", employee_l_2_2[1]));
            var employee_3_3_1 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 3 1 {x}", employee_3_1_1[0]));
            var employee_3_3_2 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 3 2 {x}", employee_3_1_1[1]));
            var employee_3_4_1 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 4 1 {x}", employee_3_1_2[0]));
            var employee_3_4_2 = Enumerable.Range(1, 2).Select(x => new Employee($"Функция 4 2 {x}", employee_3_1_2[1]));
            list.AddRange(employee_l_1);
            list.AddRange(employee_2_1);
            list.AddRange(employee_l_2_2);
            list.AddRange(employee_3_1_1);
            list.AddRange(employee_3_1_2);
            list.AddRange(employee_3_2_1);
            list.AddRange(employee_3_2_2);
            list.AddRange(employee_3_3_1);
            return list;

        }
    }

    public class UsersInformation
    {
        Dictionary<string, UserActionInfoStorage> userInformation = new Dictionary<string, UserActionInfoStorage>();

        public UsersInformation()
        {
            var brager17InfoStorage = new UserActionInfoStorage
            {
                UserName = "free_town"
            };
            userInformation = new Dictionary<string, UserActionInfoStorage>
            {
                {"free_town", brager17InfoStorage}
            };
        }

        public UserActionInfoStorage GetData(string username)
        {
            return userInformation[username];
        }
    }
}