using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotOnStateMachine.Commands
{
   public  interface ISender
    {
        void Send(string message, IReplyMarkup buttons=null);
        void Send(string message);

        void SendDeviation(DeviationCard deviationCard);
    }
}