using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotOnStateMachine.Commands
{
public class Sender : ISender
    {
        private readonly long ChatId;
        private readonly TelegramBotClient Client;

        public Sender(long chatId, TelegramBotClient client)
        {
            ChatId = chatId;
            Client = client;
        }

        public void Send(string message)
        {
            Client.SendTextMessageAsync(ChatId, message);
        }

        public void SendDeviation(DeviationCard deviationCard)
        {
            return;
        }

        public void Send(string message, IReplyMarkup buttons)
        {
            Client.SendTextMessageAsync(ChatId, message, replyMarkup: buttons);
        }
    }
}