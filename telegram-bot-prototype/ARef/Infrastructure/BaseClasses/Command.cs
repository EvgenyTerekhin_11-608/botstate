using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot.Types;

namespace BotOnStateMachine.Commands
{
    public abstract class Command
    {
        protected  readonly DataSet DataSet;
        protected readonly ISender Sender;

        protected Command(
            DataSet dataSet,
            ISender sender)
        {
            DataSet = dataSet;
            Sender = sender;
        }

        public abstract void Execute(Update update);
    }
}