using System;
using BotOnStateMachine.ARef.Infrastructure;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.Commands
{
    public class TransactionManager
    {
        private readonly StateType StateType;
        private readonly ActionStorageWrapper Wrapper;

        public TransactionManager(
            StateType stateType,
            ActionStorageWrapper wrapper)
        {
            StateType = stateType;
            Wrapper = wrapper;
        }

        public void ChangeState()
        {
            Wrapper.Set<CurrentStateInfo>().ChangeState(StateType);
        }
    }
}