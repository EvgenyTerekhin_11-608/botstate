using System.Collections.Generic;
using System.Linq;
using BotOnStateMachine.ARef.Infrastructure.Storage;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramStateMachine.CustomValidators;
using TelegramStateMachine.Infrastructure.Validators;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.Commands
{
    public class CommandBase
    {
        public readonly List<PredicateValidator> Validators;
        public readonly ContainsValidator ContainsValidator;
        private readonly TransactionManager TransactionManager;
        public List<UseMe> UseMeCommands { get; set; }

        public List<Command> Commands { get; set; }

        public CommandBase(
            List<UseMe> useMeCommands,
            List<Command> commands,
            List<PredicateValidator> validators,
            ContainsValidator containsValidator,
            TransactionManager transactionManager)
        {
            TransactionManager = transactionManager;
            UseMeCommands = useMeCommands;
            Commands = commands;
            ContainsValidator = containsValidator;
            Validators = validators;
        }

        public void ChangeState()
        {
            TransactionManager.ChangeState();
        }

        public void ExecuteCommands(Update update)
        {
            Commands.ForEach(x => x.Execute(update));
        }
    }
}