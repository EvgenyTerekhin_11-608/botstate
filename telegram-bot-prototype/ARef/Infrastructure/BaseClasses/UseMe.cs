using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot.Types;

namespace BotOnStateMachine.Commands
{
    public abstract class UseMe
    {
        protected readonly DataSet DataSet;

        protected UseMe(DataSet dataSet)
        {
            DataSet = dataSet;
        }

        public abstract bool Check(Update update);
    }
}