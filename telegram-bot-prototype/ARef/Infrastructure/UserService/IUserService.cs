namespace BotOnStateMachine.ARef.Infrastructure
{
    public interface IUserService
    {
        string Username();
    }
}