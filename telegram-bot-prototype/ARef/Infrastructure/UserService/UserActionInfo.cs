using System;
using System.Collections.Generic;
using System.Linq;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class UserActionInfoStorage : Storage.Storage
    {
        public string UserName { get; set; }
        public CurrentStateInfo CurrentStateInfo { get; set; } = new CurrentStateInfo();
        public DeviationCard deviationCard { get; set; } = new DeviationCard();
    }

    public class DeviationCard
    {
        public void FixFunction(Function f)
        {
            FunctionDto = f;
        }

        public void FixFunction(Employee e)
        {
            EmployeeInf = e;
        }

        public void FixText(string text)
        {
            Text = text;
        }

        public void ClearAll()
        {
            FunctionDto = null;
            EmployeeInf = null;
            Text = null;
        }

        public Function FunctionDto { get; set; }
        public Employee EmployeeInf { get; set; }

        public string Text { get; set; }
    }

    public class FunctionDto
    {
        public string Name { get; set; }
    }

    public class EmployeeInf
    {
        public string UserName { get; set; }
    }

    public class CurrentStateInfo
    {
        public StateType Type { get; private set; } = StateType.UnAuth;

        public void ChangeState(StateType newState)
        {
            Type = newState;
        }
    }
}