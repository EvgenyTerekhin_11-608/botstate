using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using BotOnStateMachine.ARef;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using TelegramStateMachine.Infrastructure.Validators;

namespace BotOnStateMachine
{
    public class SpeedSheetApi
    {
        static string[] Scopes = {SheetsService.Scope.SpreadsheetsReadonly};
        static string ApplicationName = "Google Sheets API .NET Quickstart";
        private SheetsService SheetService { get; set; }
        const string spreadsheetId = "131zLdmqwqGCQyxbPjkiqdxqQTZbMVTGhTCRlZ1VNF1M";

        private IEnumerable<string> GetSheetsName()
        {
            var sheets = SheetService.Spreadsheets.Get(spreadsheetId).Execute();
            return sheets.Sheets.Select(x => x.Properties.Title);
        }

        public List<StateInfo> GetStateInfos(string stateName)
        {
            var range = $"{stateName}!A2:E";
            var request =
                SheetService.Spreadsheets.Values.Get(spreadsheetId, range).Execute();
            var list = new List<StateInfo>();
            request.Values.ToList().ForEach(x =>
            {
                var inf = new StateInfo
                {
                    UseMeCheckers = x[0].ToString().Split(',').ToList(),
                    Commands = x[1].ToString().Split(',').ToList(),
                    FromToState = $"{x[2]}",
                    Validators = x[3]?.ToString()?.Split(',')?.ToList(),
                    ContainsMessage = x.Count < 5
                        ? ""
                        : $"{x[4]}"
                };
                list.Add(inf);
            });
            return list;
        }

        public SpeedSheetApi()
        {
            UserCredential credential;
            const string credPath = "token.json";
            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            // Create Google Sheets API service.
            SheetService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
        }
    }
}