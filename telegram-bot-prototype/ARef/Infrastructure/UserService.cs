using System.Collections.Generic;

namespace BotOnStateMachine.ARef.Infrastructure
{
    public class UserService : IUserService
    {
        public string Username { get; set; }

        public UserService(string username)
        {
            Username = username;
        }

        string IUserService.Username()
        {
            return Username;
        }
    }
}