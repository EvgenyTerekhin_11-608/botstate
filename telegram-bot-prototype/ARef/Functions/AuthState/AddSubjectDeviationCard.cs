using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions
{
    public class AddSubjectDeviationCard : Command
    {
        public AddSubjectDeviationCard(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var card = DataSet.ActionServiceSet<DeviationCard>();
            card.FixText(update.GetMessage());
        }
    }
}