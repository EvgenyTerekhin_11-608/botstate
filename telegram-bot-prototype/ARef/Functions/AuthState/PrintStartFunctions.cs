using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Google.Apis.Sheets.v4.Data;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions
{
    public class PrintStartFunctions : Command
    {
        private readonly KeyboardBuilder KeyboardBuilder;

        public override void Execute(Update update)
        {
            var names = DataSet.InfoServiceSetCollection<Function>().Where(x => x.ParentFunction == null)
                .Select(x => x.Name).ToList();

            var keyboard = KeyboardBuilder.Build(names);
            Sender.Send("Выберите функцию", keyboard);
        }

        public PrintStartFunctions(DataSet dataSet, ISender sender,KeyboardBuilder keyboardBuilder) : base(dataSet, sender)
        {
            KeyboardBuilder = keyboardBuilder;
        }
    }
}