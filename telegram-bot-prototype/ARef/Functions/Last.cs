﻿using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions
{
    public class Last:Command
    {
       
//        public override void Execute(string message)
//        {
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Сообщение не удалось раcпознать");
//        }

        public override void Execute(Update update)
        {
            Sender.Send("Команда не распознана");
        }

        public Last(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }
    }
}