using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot.Types;

namespace BotOnStateMachine.ARef.Functions
{
    public class PrintMyActivity : Command
    {
        public PrintMyActivity(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var deviationCard = DataSet.GetDeviationCard();
            Sender.Send(
                $"В данный момент вы фиксируете нарушение функции {deviationCard.FunctionDto?.Name} у сотрудника {deviationCard.EmployeeInf?.Name} с текстом нарушения {deviationCard.Text}");
        }
    }
}