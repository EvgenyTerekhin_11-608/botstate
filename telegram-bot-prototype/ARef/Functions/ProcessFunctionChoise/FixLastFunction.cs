using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessFunctionChoise
{
    public class FixLastFunction : Command
    {
       

//        var function = _context.Functions
//                .Include(x => x.ChildFunction)
//                .FirstOrDefault(x => x.Text == message);
//            if (function == null)
//        return false;
//        return !function.ChildFunction.Any() && function.ParentFunction != null;
//        public override void Execute(string message)
//        {
//            var function = _context.Functions.First(x => x.Text == message);
//
//            _user.SubjectDeviationCard.ChooseFunction = function;
//          
//        }

        public FixLastFunction(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var function = DataSet.InfoServiceSetCollection<Function>().FirstOrDefault(x=>x.Name == update.GetMessage());
            DataSet.ActionServiceSet<DeviationCard>().FixFunction(function);
        }
    }
}