using System.Collections.Generic;
using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessFunctionChoise
{
    public class ChooseFixOrBackPrint : Command
    {
        private readonly KeyboardBuilder KeyboardBuilder;


//        public override void Execute(string message)
//        {
//            var deviationCard = _context.DeviationCards.Include(x => x.SubjectUser)
//                .First(x => x.SubjectUser.Id == _user.Id);
//
//            var buttons = new List<InlineKeyboardButton>()
//            {
//                InlineKeyboardButton.WithCallbackData("Зафиксировать"),
//                InlineKeyboardButton.WithCallbackData("Назад")
//            };
//            _client.SendTextMessageAsync(_user.TelegramUserId,
//                $"Функция {deviationCard.ChooseFunction.Text}; Текст {deviationCard.Text}",
//                replyMarkup: new InlineKeyboardMarkup(buttons.ToTeil(1)));
////        }

        public ChooseFixOrBackPrint(DataSet dataSet, ISender sender,KeyboardBuilder keyboardBuilder) : base(dataSet, sender)
        {
            KeyboardBuilder = keyboardBuilder;
        }

        public override void Execute(Update update)
        {
            var keyboard = KeyboardBuilder.Build(new[] {"Зафиксировать", "Назад"});
            var selectedFunction = DataSet.SelectedFunction();
            var wrotenText = DataSet.WrotenText();
 
            Sender.Send($"Фукнция {selectedFunction.Name} Текст {wrotenText}",keyboard);
        }
    }
}