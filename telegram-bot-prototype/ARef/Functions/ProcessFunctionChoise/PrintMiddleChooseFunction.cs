﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessFunctionChoise
{
    public class PrintMiddleChooseFunction : Command
    {
//        public override void Execute(string message)
//        {
//            var childFunctionTexts = _user.SubjectDeviationCard.ChooseFunction.ChildFunction
//                .Select(x => InlineKeyboardButton.WithCallbackData(x.Text))
//                .ToList();
//
//            childFunctionTexts.Add(InlineKeyboardButton.WithCallbackData("Назад"));
//
//            var keyboard = new InlineKeyboardMarkup(childFunctionTexts.ToTeil(3));
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Выберите функцию", replyMarkup: keyboard);
//        }

        public PrintMiddleChooseFunction(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var selectedFunction = DataSet.SelectedFunction();
            var children = DataSet.InfoServiceSetCollection<Function>()
                .Where(x => x.ParentFunction == selectedFunction);
            var buttons = children.Select(x => InlineKeyboardButton.WithCallbackData(x.Name))
                .ToList();
            buttons.Add(InlineKeyboardButton.WithCallbackData("Назад"));
            var keyboard = new InlineKeyboardMarkup(buttons.ToTeil(3));
            Sender.Send("Выберите функцию", keyboard);
        }
    }
}