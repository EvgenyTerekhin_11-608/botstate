using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessFunctionChoise
{
    public class BackToStartFixFunction:Command
    {
      

        //вывети кнопки первых функций

//        public override void Execute(string message)
//        {
//            _user.SubjectDeviationCard.ChooseFunction = null;
//        }

        public BackToStartFixFunction(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            DataSet.ActionServiceSet<DeviationCard>().FunctionDto = null;
        }
    }
}