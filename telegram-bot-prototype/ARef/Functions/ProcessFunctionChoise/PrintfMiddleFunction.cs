using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions
{
    public class PrintMiddleFunction : Command
    {
//        public override void Execute(string message)
//        {
//            var childFunctionTexts = _context.Functions
//                .Include(x => x.ChildFunction)
//                .First(x => x.Text == message)
//                .ChildFunction
//                .Select(x => InlineKeyboardButton.WithCallbackData(x.Text))
//                .ToList();
//
//            childFunctionTexts.Add(InlineKeyboardButton.WithCallbackData("Назад"));
//           
//            var keyboard = new InlineKeyboardMarkup(childFunctionTexts.ToTeil(3));
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Выберите функцию", replyMarkup: keyboard);
//        }
        public PrintMiddleFunction(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var selectedFunction = DataSet.ActionServiceSet<DeviationCard>().FunctionDto;
            var printbuttons = DataSet.InfoServiceSetCollection<Function>()
                .Where(x => x.ParentFunction?.Name == selectedFunction.Name)
                .Select(x => InlineKeyboardButton.WithCallbackData(x.Name));
            var keyboard = new InlineKeyboardMarkup(printbuttons.ToTeil(3));
            Sender.Send("Выберите функцию", keyboard);
        }
    }
}