﻿using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class ClearDeviationCard : Command
    {
        public ClearDeviationCard(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            DataSet.GetDeviationCard().ClearAll();
        }
    }
}