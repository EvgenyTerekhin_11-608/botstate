﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class FixEmployeeInDeviationCard : Command
    {
//        public override void Execute(string message)
//        {
//            var user = _context.Users.First(x => x.UserName.Contains(message));
//            _user.SubjectDeviationCard.ObjectUser = user;
//        }
        public FixEmployeeInDeviationCard(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var employee = DataSet.InfoServiceSetCollection<Employee>()
                .FirstOrDefault(x => x.Name.Contains(update.GetMessage()));
            DataSet.ActionServiceSet<DeviationCard>().EmployeeInf = employee;
        }
    }
}