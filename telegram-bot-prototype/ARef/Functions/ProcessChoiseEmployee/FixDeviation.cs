﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class FixDeviation : Command
    {

//        public override void Execute(string message)
//        {
//            var userName = message;
//            var objectAssignee = _user.SubjectDeviationCard.ObjectUser;
//            var deviationCard = _user.SubjectDeviationCard;
//            var function = deviationCard.ChooseFunction;
//            var text = deviationCard.Text;
//
//            _user.AddDeviation(objectAssignee,text,function);
//        }
        public FixDeviation(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            Sender.SendDeviation(DataSet.GetDeviationCard());
        }
    }
}