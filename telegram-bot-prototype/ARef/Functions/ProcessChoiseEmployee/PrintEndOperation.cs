﻿using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class PrintEndOperation : Command
    {
     
//        public override void Execute(string message)
//        {
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Зафиксировано");
//        }

        public override void Execute(Update update)
        {
          Sender.Send("Зафиксировано");
        }

        public PrintEndOperation(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }
    }
}