﻿using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class PrintNotExistsEmployee : Command
    {
//        public override void Execute(string message)
//        {
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Ни одного сотрудника не найдено");
//        }

        public PrintNotExistsEmployee(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            Sender.Send("Ни одного сотрудника не найдено");
        }
    }
}