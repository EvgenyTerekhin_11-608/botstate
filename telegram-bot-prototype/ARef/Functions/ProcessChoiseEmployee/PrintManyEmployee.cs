﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class PrintManyEmployee : Command
    {
        private readonly KeyboardBuilder KeyboardBuilder;
//        public override void Execute(string message)
//        {
//            var users = _context.Users.Where(x => x.UserName.Contains(message)).ToList();
//            var buttons = users.Select(x => InlineKeyboardButton.WithCallbackData(x.UserName)).ToTeil(3);
//            var keyboard = new InlineKeyboardMarkup(buttons);
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Выбрано несколько сотрудников,отметьте нужного",
//                replyMarkup: keyboard);
//        }

        public PrintManyEmployee(DataSet dataSet, ISender sender,KeyboardBuilder keyboardBuilder) : base(dataSet, sender)
        {
            KeyboardBuilder = keyboardBuilder;
        }

        public override void Execute(Update update)
        {
            var employees = DataSet.InfoServiceSetCollection<Employee>();
            var manes = employees.Select(x => x.Name);
            var keyboard = KeyboardBuilder.Build(manes);
            Sender.Send("Выбрано несколько сотрудников,отметьте нужного", keyboard);
        }
    }
}