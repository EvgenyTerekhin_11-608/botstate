﻿using System.Collections.Generic;
using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessChoiseEmployee
{
    public class PrintConfidenceInСhoice : Command
    {
//        public override void Execute(string message)
//        {
//            var userName = message;
//            var objectAssignee = _context.Users.First(x => x.UserName == userName);
//            var deviationCard = _user.SubjectDeviationCard;
//            var function = deviationCard.ChooseFunction;
//            var text = deviationCard.Text;
//
//            var buttons = new List<InlineKeyboardButton>()
//            {
//                InlineKeyboardButton.WithCallbackData("Зафиксировать"),
//                InlineKeyboardButton.WithCallbackData("Перевыбрать сотрудника"),
//            };
//            _client.SendTextMessageAsync(_user.TelegramUserId,
//                $"Действие завершено, проверьте данные: Функция-{function.Text} Нарушитель-{objectAssignee.UserName} Текст-{text}",
//                replyMarkup: new InlineKeyboardMarkup(buttons));
//        }
        public PrintConfidenceInСhoice(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var userName = update.GetMessage();
            var objectAssignee = DataSet.InfoServiceSetCollection<Employee>().First(x => x.Name == userName);
            var deviationCard = DataSet.GetDeviationCard();
            var function = deviationCard.FunctionDto;
            var text = deviationCard.Text;

            var buttons = new List<InlineKeyboardButton>()
            {
                InlineKeyboardButton.WithCallbackData("Зафиксировать"),
                InlineKeyboardButton.WithCallbackData("Перевыбрать сотрудника"),
            };
            Sender.Send(
                $"Действие завершено, проверьте данные: Функция-{function.Name} Нарушитель-{objectAssignee.Name} Текст-{text}",
                new InlineKeyboardMarkup(buttons));
        }
    }
}