﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotOnStateMachine.ARef.Functions
{
    public class PrintContainsChoose : Command
    {
//        public override void Execute(string message)
//        {
//            var users = _context.Users.ToList();
//            var buttons = users.Select(x => InlineKeyboardButton.WithCallbackData(x.UserName)).ToTeil(3);
//            var keyboard = new InlineKeyboardMarkup(buttons);
//
//            _client.SendTextMessageAsync(_user.TelegramUserId, "Выберите сотрудника", replyMarkup: keyboard);
//        }

        public PrintContainsChoose(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var users = DataSet.InfoServiceSetCollection<Employee>();
            var buttons = users.Select(x => InlineKeyboardButton.WithCallbackData(x.Name)).ToTeil(3);
            var keyboard = new InlineKeyboardMarkup(buttons);

            Sender.Send("Выберите сотрудника", keyboard);
        }
    }
}