﻿using System;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot.Types;

namespace BotOnStateMachine.ARef.Functions
{
    public class Cancel:Command
    {
       
//        public override void Execute(string message)
//        {
//            var subjectDeviationCard = _user.SubjectDeviationCard;
//            _context.Remove(subjectDeviationCard);
//        }

        public Cancel(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            DataSet.ActionServiceSet<DeviationCard>().ClearAll();
        }
    }
}