using System.Threading.Tasks;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.Functions
{
    public class Authorize : Command
    {
//        public override void Execute(string message)
//        {
//            _user.Authorize();
//            Task.Run(() => _client.SendTextMessageAsync(_user.TelegramUserId, "Вы успешно авторизованы"));
//        }

        public Authorize(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            Sender.Send("Вы успешно зарегистрировались");
        }
    }
}