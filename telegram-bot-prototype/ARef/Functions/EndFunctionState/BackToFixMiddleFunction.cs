using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessFunctionChoise
{
    public class BackToFixMiddleFunction:Command
    {
       

        //Еще нужно вывести кнопки
        //        var lastChoosedFunction = _user.SubjectDeviationCard.ChooseFunction;
//            return message.Contains("Назад") && lastChoosedFunction.ParentFunction != null;
//        public override void Execute(string message)
//        {
//            _user.SubjectDeviationCard.ChooseFunction = _user.SubjectDeviationCard.ChooseFunction.ParentFunction;
//        }
        public override void Execute(Update update)
        {
            var selectedFunction = DataSet.SelectedFunction();
            DataSet.GetDeviationCard().FixFunction(selectedFunction.ParentFunction);

        }

        public BackToFixMiddleFunction(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }
    }
}