﻿using System.Collections.Generic;
using System.Security.Policy;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.ProcessFunctionChoise
{
    public class PrintChooiseEmployee : Command
    {
        private readonly KeyboardBuilder KeyboardBuilder;

//        public PrintChooiseEmployee(TelegramBotContext context, TelegramBotClient client, User user) : base(context, client,
//            user)
//        {
//        }
//
//        public override void Execute(string message)
//        {
//            var buttons = new List<InlineKeyboardButton>
//            {
//                InlineKeyboardButton.WithCallbackData("Ввести"),
//                InlineKeyboardButton.WithCallbackData("Выбрать")
//            };
//            _client.SendTextMessageAsync(_user.TelegramUserId,
//                "Функция зафиксирована, Вы хотите ввести имя сотрудника или выбрать из списка?",
//                replyMarkup: new InlineKeyboardMarkup(buttons));
//        }
        public PrintChooiseEmployee(DataSet dataSet, ISender sender,KeyboardBuilder keyboardBuilder) : base(dataSet, sender)
        {
            KeyboardBuilder = keyboardBuilder;
        }

        public override void Execute(Update update)
        {
            Sender.Send("Функция зафиксирована, Вы хотите ввести имя сотрудника или выбрать из списка?",
               KeyboardBuilder.Build(new[]{"Ввести", "Выбрать"}));
        }
    }
}