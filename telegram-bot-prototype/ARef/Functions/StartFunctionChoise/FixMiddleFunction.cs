using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.Functions.StartFunctionChoise
{
    public class FixMiddleFunction : Command
    {
//        public override void Execute(string message)
//        {
//            var function = _context.Functions
//                .Include(x => x.ChildFunction)
//                .First(x => x.Text == message);
//            var deviationCard = _context.DeviationCards.Include(x => x.SubjectUser)
//                .First(x => x.SubjectUser.Id == _user.Id);
//            deviationCard.ChooseFunction = function;
//            
//        }
        public FixMiddleFunction(DataSet dataSet, ISender sender) : base(dataSet, sender)
        {
        }

        public override void Execute(Update update)
        {
            var function = DataSet.InfoServiceSetCollection<Function>().First(x => x.Name == update.GetMessage());
            DataSet.ActionServiceSet<DeviationCard>().FixFunction(function);
        }
    }
}