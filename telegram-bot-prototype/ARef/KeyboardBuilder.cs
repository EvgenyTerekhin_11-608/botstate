using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using Telegram.Bot.Types.ReplyMarkups;

namespace BotOnStateMachine.ARef
{
    public class KeyboardBuilder
    {
        public KeyboardBuilder()
        {
        }

        public InlineKeyboardMarkup Build(IEnumerable<string> names)
        {
            return new InlineKeyboardMarkup(names.Select(InlineKeyboardButton.WithCallbackData).ToTeil(2).ToList());
        }
    }
}