using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;

namespace BotOnStateMachine.ARef
{
    public static class UpdateExtention
    {
        public static string GetMessage(this Update Update)
        {
            var message = Update.Message?.Text
                          ?? Update.CallbackQuery.Data;
            return message;
        }
    }
}