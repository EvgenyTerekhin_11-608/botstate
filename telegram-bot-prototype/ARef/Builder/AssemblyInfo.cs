using System;
using System.Collections.Generic;
using System.Linq;

namespace BotOnStateMachine.Infrastucture
{
    public class AssemblyInfo
    {
        public List<Type> AllStateClasses { get; }

        public AssemblyInfo()
        {
            AllStateClasses = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(x => !x.IsDynamic)
                .SelectMany(x => x.GetExportedTypes())
                .OrderBy(x => x.Name)
                .ToList();
        }

        public Type GetTypeByName(string fullname, bool byFullname= true)
        {
            Type typeByName = null;
            if (byFullname)
            {
                 typeByName = AllStateClasses.FirstOrDefault(x => x.FullName == fullname);
            }
            else
            {
                typeByName = AllStateClasses.FirstOrDefault(x => x.Name == fullname);
            }

            if (typeByName == null)
            {
            }

            return typeByName;
        }

        public IEnumerable<Type> GetTypeByName(IEnumerable<string> names,bool byFullname = true)
        {
            return names.Select(x=>GetTypeByName(x,byFullname));
        }
    }
}