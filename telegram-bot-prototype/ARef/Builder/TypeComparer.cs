using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using Google.Apis.Sheets.v4.Data;
using Microsoft.EntityFrameworkCore.Internal;

namespace BotOnStateMachine.Infrastucture
{
    public class TypeComparer : IComparer<Type>
    {
        private List<Type> types;

        public TypeComparer(IEnumerable<Type> types)
        {
            this.types = types.ToList();
        }

        public int Compare(Type x, Type y)
        {
            x = TryReplaceImplementInterface(x);
            y = TryReplaceImplementInterface(y);
            var xPosition = types.IndexOf(x);
            var yPosition = types.IndexOf(y);
            if (xPosition > yPosition)
                return 1;
            if (xPosition < yPosition)
                return -1;
            return 0;
        }

        Type TryReplaceImplementInterface(Type x)
        {
            if (x.GetInterfaces().Any(xx=>types.Contains(xx)) && !types.Contains(x))
            {
               return x.GetInterfaces().First(xx => types.Contains(xx));
            }

            return x;
        }
    }
}