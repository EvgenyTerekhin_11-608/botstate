using System;
using System.Collections.Concurrent;

namespace BotOnStateMachine.Infrastucture
{
    public class Cache<TKey, TValue>
    {
        private ConcurrentDictionary<TKey, TValue> cache = new ConcurrentDictionary<TKey, TValue>();

        public void Add(TKey key, TValue value)
        {
            cache.TryAdd(key, value);
        }

        public TValue TryGetValue(TKey key)
        {
            cache.TryGetValue(key, out var value);
            return value;
        }

        public TValue GetOrDo(TKey key, Func<TKey, TValue> func)
        {
            var founded = cache.TryGetValue(key, out var value);
            if (founded) return value;
            value = func(key);
            Add(key, value);
            return value;
        }
    }
}