using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mime;
using System.Reflection;
using System.Xml.Linq;
using BotOnStateMachine.ARef;
using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using Telegram.Bot;
using TelegramStateMachine.CustomValidators;
using TelegramStateMachine.Infrastructure.Validators;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.Infrastucture
{
    public class StateBuilder
    {
        private readonly SpeedSheetApi SheetApi;
        private readonly UserService UserService;
        private readonly ActionStorageWrapper ActionStorageWrapper;
        private readonly InfoStorageWrapper InfoStorageWrapper;
        private readonly DataSet Dataset;
        private readonly Builder Builder;
        private readonly TelegramBotClient _client;
        private AssemblyInfo _assemblyInfo { get; }
        private string StateName { get; }


        private List<object> UseMeParameters = new List<object>();
        private List<object> CommandParameters = new List<object>();

        public StateBuilder(
            SpeedSheetApi sheetApi,
            string stateName,
            Sender Sender,
            UserService userService,
            ActionStorageWrapper ActionStorageWrapper,
            InfoStorageWrapper infoStorageWrapper,
            DataSet dataset,
            Builder builder)
        {
            StateName = stateName;
            SheetApi = sheetApi;
            UserService = userService;
            this.ActionStorageWrapper = ActionStorageWrapper;
            InfoStorageWrapper = infoStorageWrapper;
            Dataset = dataset;
            Builder = builder;
            _assemblyInfo = new AssemblyInfo();
            UseMeParameters = new List<object>
            {
                Dataset
            };
            CommandParameters = new List<object>
            {
                Dataset, Sender
            };
        }

        public State GetState()
        {
            var StateInfos = SheetApi.GetStateInfos(StateName);
            var commandBases = StateInfos.Select(BuildCommand).ToList();
            var state = new State(commandBases);
            return state;
        }

        CommandBase BuildCommand(StateInfo info)
        {
            var useme = UseMeBuild(info.UseMeCheckers);
            var command = CommandBuild(info.Commands);
            var transaction = TransactionManagerBuild(info.FromToState);
            var validators = ValidatorsBuild(info.Validators);
            var containsValidator = ContainsValidatorBuild(info.ContainsMessage);
            return new CommandBase(useme, command, validators, containsValidator, transaction);
        }

        ContainsValidator ContainsValidatorBuild(string str)
        {
            var result = new ContainsValidator(str);
            return result;
        }

        List<PredicateValidator> ValidatorsBuild(List<string> validators)
        {
            var result = validators.Select(x => Builder.Build(x, new List<object>())).Cast<PredicateValidator>().ToList();
            return result;
        }

        List<UseMe> UseMeBuild(List<string> useMe)
        {
            var result = useMe.Select(x => Builder.Build(x, UseMeParameters)).Cast<UseMe>().ToList();
            return result;
        }

        List<Command> CommandBuild(List<string> commands)
        {
            var result = commands.Select(x => Builder.Build(x, CommandParameters)).Cast<Command>().ToList();
            return result;
        }

        TransactionManager TransactionManagerBuild(string state)
        {
            return new TransactionManager(Enum.Parse<StateType>(state), ActionStorageWrapper);
        }
    }
}