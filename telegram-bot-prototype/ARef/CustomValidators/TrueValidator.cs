using System;
using Telegram.Bot.Types;
using TelegramStateMachine.Infrastructure.Validators;

namespace TelegramStateMachine.CustomValidators
{
    public class TrueValidator:PredicateValidator
    {
        public TrueValidator() : base(x=>true)
        {
        }
    }
}