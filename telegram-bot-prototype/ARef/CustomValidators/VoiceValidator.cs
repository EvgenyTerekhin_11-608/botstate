using BotOnStateMachine.Commands;
using TelegramStateMachine.Infrastructure.Validators;

namespace TelegramStateMachine.CustomValidators
{
    public class VoiceValidator : PredicateValidator
        {
            public VoiceValidator() : base(x => x.Message?.Voice != null)
            {
            }
        }
}