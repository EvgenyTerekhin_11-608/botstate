using BotOnStateMachine.Commands;
using TelegramStateMachine.Infrastructure.Validators;

namespace TelegramStateMachine.CustomValidators
{
    public class ButtonValidator : PredicateValidator
    {
        public ButtonValidator() : base(x => x?.CallbackQuery?.Data != null)
        {
        }
    }
}