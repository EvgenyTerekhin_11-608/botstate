using BotOnStateMachine.Commands;
using TelegramStateMachine.Infrastructure.Validators;

namespace TelegramStateMachine.CustomValidators
{
    public class TextValidator : PredicateValidator
    {
        public TextValidator() : base(x => x?.Message?.Text != null)
        {
        }
    }
}