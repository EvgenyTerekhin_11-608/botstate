namespace Telegram_Bot_Machine.Models
{
    public enum StateType
    {
        UnAuth,
        Auth,
        StartFunctionChoose,
        ProcessFunctionChoose,
        EndFunctionChoose,
        StartEmployeeChoose,
        ProcessEmployeeChoise,
        ChooseEmp,
        
    }
}