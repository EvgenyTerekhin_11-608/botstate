﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using BotOnStateMachine.Commands;
using Telegram.Bot.Types;

namespace BotOnStateMachine
{
    public class State
    {
        private IEnumerable<CommandBase> StateActions { get; }

        public State(IEnumerable<CommandBase> Commands)
        {
            StateActions = Commands;
        }

        public void Execute(Update update)
        {
            var command = StateActions.First(x =>
                x.Validators.Aggregate(false, (a, c) => a || c.isValid(update)) &&
                x.ContainsValidator.isValid(update) &&
                x.UseMeCommands.Aggregate(true, (a, c) => a && c.Check(update)));

            command.ExecuteCommands(update);
            command.ChangeState();
        }
    }
}