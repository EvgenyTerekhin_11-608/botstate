using Telegram.Bot.Types;

namespace TelegramStateMachine.Infrastructure.Validators
{
    public interface IValidator
    {
        bool isValid(Update update);
    }
}