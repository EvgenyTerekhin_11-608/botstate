using System;
using Google.Apis.Sheets.v4.Data;
using Telegram.Bot.Types;

namespace TelegramStateMachine.Infrastructure.Validators
{
    public class PredicateValidator : IValidator
    {
        private readonly Func<Update, bool> _predicate;

        public PredicateValidator(Func<Update, bool> predicate)
        {
            _predicate = predicate;
        }

        public bool isValid(Update update)
        {
            var result = _predicate(update);
            return result;
        }
    }
}