﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class HasChildren : Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
            var function = DataSet.InfoServiceSetCollection<Function>().FirstOrDefault(x => x.Name == update.GetMessage());
            if (function == null)
                return false;
            if (DataSet.InfoServiceSetCollection<Function>().Any(x => x.ParentFunction?.Name == function.Name))
                return true;
            return false;
        }

        public HasChildren(DataSet dataSet) : base(dataSet)
        {
        }
    }
}