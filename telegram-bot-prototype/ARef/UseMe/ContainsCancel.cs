﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ContainsCancel : Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
            return update.Message.Contains("/cancel");
        }

        public ContainsCancel(DataSet dataSet) : base(dataSet)
        {
        }
    }
}