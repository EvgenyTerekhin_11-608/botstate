﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Google.Apis.Sheets.v4.Data;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class HasNotContainsChildrenChooseFunction : Commands.UseMe
    {
       
       

        public override bool Check(Update update)
        {
//            var function = _user.SubjectDeviationCard.ChooseFunction;
//            if (function==null)
//                return false;
//
//            var check = function.ChildFunction == null || !function.ChildFunction.Any();
//            return check;
            var selectedFunction = DataSet.SelectedFunction();
            if (selectedFunction == null)
                return false;
            var children = DataSet.InfoServiceSetCollection<Function>()
                .Where(x => x.ParentFunction == selectedFunction);
            var check = !children.Any();
            return check;
        }

        public HasNotContainsChildrenChooseFunction(DataSet dataSet) : base(dataSet)
        {
        }
    }
}