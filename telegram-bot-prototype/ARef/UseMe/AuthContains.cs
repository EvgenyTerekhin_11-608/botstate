﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class AuthContains : Commands.UseMe
    {
//        public override bool Check(string message) => message.Contains("/auth");


       

        public override bool Check(Update update)
        {
            return update.Message.Contains("/auth");
        }

        public AuthContains(DataSet dataSet) : base(dataSet)
        {
        }
    }
}