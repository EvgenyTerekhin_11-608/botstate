﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ContainsChoose : Commands.UseMe
    {
        public override bool Check(Update update)
        {
            return update.GetMessage().Contains("Выбрать");
        }


        public ContainsChoose(DataSet dataSet) : base(dataSet)
        {
        }
    }
}