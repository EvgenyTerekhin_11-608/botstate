﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ParentNull : Commands.UseMe
    {

        public override bool Check(Update update)
        {
            /*var function = _context.Functions.Include(x => x.ParentFunction).FirstOrDefault(x => x.Text == message);
            if (function == null)
                return false;

            var check = function.ParentFunction == null;
            return check;*/
            return false;
        }

        public ParentNull(DataSet dataSet) : base(dataSet)
        {
        }
    }
}