﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot.Types;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ContainsBack : Commands.UseMe
    {
        
        public override bool Check(Update update)
        {
            return update.GetMessage().Contains("Назад");
        }


        public ContainsBack(DataSet dataSet) : base(dataSet)
        {
        }
    }
}