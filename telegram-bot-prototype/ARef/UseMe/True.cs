﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class True : Commands.UseMe
    {

        public override bool Check(Update update) => true;

        public True(DataSet dataSet) : base(dataSet)
        {
        }
    }
}