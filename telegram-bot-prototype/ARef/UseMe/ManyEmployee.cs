﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ManyEmployee : Commands.UseMe
    {

       

        public override bool Check(Update update)
        {
//            var result = _context.Users.Count(x => x.UserName.Contains(message)) > 1;
//            return result;
            var employees = DataSet.InfoServiceSetCollection<Employee>()
                .Count(x => x.Name == update.GetMessage());
            return employees > 1;
        }

        public ManyEmployee(DataSet dataSet) : base(dataSet)
        {
        }
    }
}