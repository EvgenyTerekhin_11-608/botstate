using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot.Types;

namespace BotOnStateMachine.ARef.UseMe
{
    public class MyActivityContains:Commands.UseMe
    {
        public MyActivityContains(DataSet dataSet) : base(dataSet)
        {
        }

        public override bool Check(Update update)
        {
            return update.GetMessage().ToLower().Contains("/myactivity");
        }
    }
}