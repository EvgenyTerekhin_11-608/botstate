﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ParentNotNull : Commands.UseMe
    {
       
      

        public override bool Check(Update update)
        {
            /*var function = _context.Functions.Include(x => x.ParentFunction).FirstOrDefault(x => x.Text == message);
            if (function == null)
                return false;

            var check = function.ParentFunction != null;
            return check;*/
            var selectedFunction = DataSet.InfoServiceSetCollection<Function>()
                .FirstOrDefault(x => x.Name == update.GetMessage());
            if (selectedFunction == null)
                return false;
            return selectedFunction.ParentFunction != null;
        }

        public ParentNotNull(DataSet dataSet) : base(dataSet)
        {
        }
    }
}