﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ParentChoiseFunctionNull : Commands.UseMe
    {
       
        public override bool Check(Update update)
        {
/*var chooseFunction = _user.SubjectDeviationCard?.ChooseFunction;
            if (chooseFunction == null)
                return false;
            return chooseFunction.ParentFunction == null;*/
            var selectedFunction = DataSet.ActionServiceSet<DeviationCard>().FunctionDto;
            return selectedFunction.ParentFunction == null;
        }

        public ParentChoiseFunctionNull(DataSet dataSet) : base(dataSet)
        {
        }
    }
}