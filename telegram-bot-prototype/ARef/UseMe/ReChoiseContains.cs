﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ContainsReChoise : Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
            return update.GetMessage().Contains("Перевыбрать сотрудника");
        }

        public ContainsReChoise(DataSet dataSet) : base(dataSet)
        {
        }
    }
}