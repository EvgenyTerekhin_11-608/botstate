﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class NotExistsEmployee : Commands.UseMe
    {
       
        public override bool Check(Update update)
        {
            /*var result = !_context.Users.Any(x => x.UserName.Contains(message));
                       return result;*/
            var employees = DataSet.InfoServiceSetCollection<Employee>()
                .Any(x => x.Name.Contains(update.GetMessage()));
            return !employees;
        }

        public NotExistsEmployee(DataSet dataSet) : base(dataSet)
        {
        }
    }
}