﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class DeviationCardEmpoyeeNotNull : Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
            //            return _user.SubjectDeviationCard.ObjectUser != null;
            var selectedAssignee = DataSet.SelectedEmployee();
            return selectedAssignee != null;
        }

        public DeviationCardEmpoyeeNotNull(DataSet dataSet) : base(dataSet)
        {
        }
    }
}