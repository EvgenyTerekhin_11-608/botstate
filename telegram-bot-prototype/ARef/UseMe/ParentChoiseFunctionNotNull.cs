﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class ParentChoiseFunctionNotNull : Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
            /*var chooseFunction = _user.SubjectDeviationCard?.ChooseFunction;
            return chooseFunction?.ParentFunction != null; */
            var selectedFunction = DataSet.SelectedFunction();
            return selectedFunction != null;
        }

        public ParentChoiseFunctionNotNull(DataSet dataSet) : base(dataSet)
        {
        }
    }
}