﻿using BotOnStateMachine.ARef.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class SendContains:Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
           return update.Message.Contains("/send");
        }

        public SendContains(DataSet dataSet) : base(dataSet)
        {
        }
    }
}