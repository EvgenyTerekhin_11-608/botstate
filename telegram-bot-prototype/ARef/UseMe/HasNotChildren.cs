﻿using System.Linq;
using BotOnStateMachine.ARef.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine.ARef.UseMe
{
    public class HasNotChildren : Commands.UseMe
    {
       

        public override bool Check(Update update)
        {
//            var function = _context.Functions.Include(x => x.ChildFunction).FirstOrDefault(x => x.Text == message);
//     if (function == null)
//                return false;
//
//            var check = !function.ChildFunction.Any();
//            return check;
            var function = DataSet.InfoServiceSetCollection<Function>().FirstOrDefault(x=>x.Name == update.GetMessage());
            var childrens = DataSet.InfoServiceSetCollection<Function>()
                .Where(x => x.ParentFunction == function);
            return !childrens.Any();
        }

        public HasNotChildren(DataSet dataSet) : base(dataSet)
        {
        }
    }
}