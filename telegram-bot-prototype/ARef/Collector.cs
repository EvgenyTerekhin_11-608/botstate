using BotOnStateMachine.ARef.Infrastructure;
using BotOnStateMachine.Commands;
using BotOnStateMachine.Infrastucture;
using Telegram.Bot;

namespace BotOnStateMachine.ARef
{
    public class Collector
    {
        private readonly TelegramBotClient BotClient;
        private Sender Sender { get; set; }
        private UserService UserService { get; set; }
        private ActionStorageWrapper ActionStorageWrapper { get; set; }
        private InfoStorageWrapper InfoStorageWrapper { get; set; }
        private long ChatId { get; set; }
        private string UserName { get; set; }
        private Builder Builder { get; set; }
        private InfoStorage InfoStorage { get; set; }
        private UsersInformation UsersInformation { get; set; }
        private SpeedSheetApi sheetApi { get; set; }

        private DataSet DataSet { get; set; }

        public Collector(TelegramBotClient botClient)
        {
            BotClient = botClient;
            Builder = new Builder();
            InfoStorage = new InfoStorage();
            UsersInformation = new UsersInformation();
            sheetApi = new SpeedSheetApi();
        }

        public State ReCreateEntities(long chatId, string userName)
        {
            Sender = new Sender(chatId, BotClient);

            UserService = new UserService(userName);
            ActionStorageWrapper = new ActionStorageWrapper(UsersInformation.GetData(userName), UserService);
            var stateName = ActionStorageWrapper.Set<CurrentStateInfo>().Type.ToString();
            InfoStorageWrapper = new InfoStorageWrapper(InfoStorage, UserService);
            var dataset = new DataSet(InfoStorageWrapper, ActionStorageWrapper);
            InfoStorageWrapper = InfoStorageWrapper;
            var StateBuilder = new StateBuilder(
                sheetApi,
                stateName,
                Sender, UserService,
                ActionStorageWrapper,
                InfoStorageWrapper, dataset, Builder);
            return StateBuilder.GetState();
        }
    }
}