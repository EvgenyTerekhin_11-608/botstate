﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BotOnStateMachine.ARef.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telegram_Bot_Machine.Models;

namespace BotOnStateMachine
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
//                Seed(dbContext);
            }

//            new SpeedSheetApi();
            new TelegramBotManager();
        }
    }

    public class StateInfo
    {
        public List<string> UseMeCheckers { get; set; }
        public List<string> Commands { get; set; }
        public List<string> Validators { get; set; }
        
        public string ContainsMessage { get; set; }
        public string FromToState { get; set; }
    }
}