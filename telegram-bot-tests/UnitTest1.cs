//using System;
//using System.Collections.Generic;
//using System.Collections.Immutable;
//using System.Data;
//using System.Diagnostics;
//using System.Linq;
//using System.Runtime.InteropServices.WindowsRuntime;
//using BotOnStateMachine;
//using BotOnStateMachine.ARef.Infrastructure;
//using BotOnStateMachine.ARef.Infrastructure.Storage;
//using BotOnStateMachine.ARef.InfrastructureStorageWrapper;
//using BotOnStateMachine.Infrastucture;
//using NUnit.Framework;
//using Telegram.Bot.Types;
//using DataSet = BotOnStateMachine.ARef.Infrastructure.DataSet;
//
//namespace Tests
//{
//    public class CustomUser
//    {
//        public string Name { get; set; }
//        public string Surname { get; set; }
//
//        public List<UserInfo> infos = Enumerable.Range(1, 10).Select((x, i) => new UserInfo {PasportId = i.ToString()})
//            .ToList();
//    }
//
//
//    public class UserInfo
//    {
//        public string PasportId { get; set; }
//    }
//
//    public partial class ReadStorage : Storage
//    {
//        public List<string> list { get; set; }
//
//        public ReadStorage()
//        {
//            list = new List<string>() {"a"};
//        }
//    }
//
//    public partial class WriteStorage : Storage
//    {
//        public CustomUser user { get; set; }
//        public CustomUser user1 { get; set; }
//
//        public WriteStorage()
//        {
//            user = new CustomUser
//            {
//                Name = "name",
//                infos = Enumerable.Range(1, 100).Select(x => new UserInfo()).ToList()
//            };
//            user1 = new CustomUser
//            {
//                Name = "name1",
//                infos = Enumerable.Range(1, 100).Select(x => new UserInfo()).ToList()
//            };
//        }
//    }
//
//    public class Sett
//    {
//        public Sett()
//        {
//        }
//
//        public string Get()
//        {
//            return "Sest";
//        }
//    }
//
//    public class TestService112424
//    {
//        private readonly CustomUser User;
//        private readonly ReadStorage Storage;
//        public readonly Sett Sett;
//
//        public TestService112424(CustomUser user, ReadStorage storage, Sett sett)
//        {
//            User = user;
//            Storage = storage;
//            Sett = sett;
//        }
//
//        public string GetUserName()
//        {
//            return User.Name;
//        }
//
//        public int GetStoreCount()
//        {
//            return Storage.list.Count;
//        }
//    }
//
//    public class Tests
//    {
//        public DataSet<ReadStorage, WriteStorage> set;
//
//        [SetUp]
//        public void Setup()
//        {
//            set = new DataSet<ReadStorage, WriteStorage>(
//                new StorageWrapper<ReadStorage>(new ReadStorage(), new UserService("name")),
//                new StorageWrapper<WriteStorage>(new WriteStorage(), new UserService("name")));
//        }
//
//        [Test]    
//        public void SetTest()
//        {
//            var list = set.ReadStorageCollection<Dog>();
//            list.First().Age = 20;
//            var list1 = set.ReadStorageCollection<Dog>();
//
//            var customUser = set.WriteStorageSet<CustomUser>("user");
//            customUser.Name = "SOSAT!!!!!!!sosat";
//            var customUser1 = set.WriteStorageSet<CustomUser>("user");
//            Assert.AreEqual(1, list.Count());
//            Assert.AreEqual("name", customUser.Name);
//            Assert.AreEqual(100, customUser1.infos.Count);
//            Assert.AreEqual("name1", customUser1.Name);
//            Assert.AreEqual(100, customUser1.infos.Count);
//        }
//
//        [Test]
//        public void DependencyResolverTest()
//        {
//            var user = new CustomUser
//            {
//                Name = "name111"
//            };
//            var builder = new Builder();
//            var storage = new ReadStorage();
//            var s = new Stopwatch();
//            var test_res = builder.Build<TestService112424>(new[] {typeof(CustomUser), typeof(ReadStorage)},
//                new List<object> {user, storage});
//
//            Assert.AreEqual("name111", test_res.GetUserName());
//            Assert.AreEqual("Sest", test_res.Sett.Get());
//            Assert.AreEqual(1, test_res.GetStoreCount());
//        }
//
//        [Test]
//        public void CreateClassByGoogleSheets()
//        {
//            var api = new GoogleSheetApi();
//            var answ = api.GetAreaInfo("Register", "A2", "B");
//            var inter = answ[0][0].ToString();
//            var pa = answ[0][1].ToString();
//            var test_res = new Builder().DeepBuild<TestService7>();
//            var test_res1 = new Builder().DeepBuild<TestService6>();
//            var test_res2 = new Builder().DeepBuild<TestService5>();
//            var test_res3 = new Builder().DeepBuild<TestService4>();
//            var test_res4 = new Builder().DeepBuild<TestService3>();
//            var test_res5 = new Builder().DeepBuild<TestService2>();
//            var test_res6 = new Builder().DeepBuild<TestService1>();
//            var test_res7 = new Builder().DeepBuild<TestService>();
//            Assert.AreEqual(test_res.Sosat(), "Сосать");
//            Assert.AreEqual(test_res1.Sosat(), "Сосать");
//            Assert.AreEqual(test_res2.Sosat(), "Сосать");
//            Assert.AreEqual(test_res3.Sosat(), "Сосать");
//            Assert.AreEqual(test_res4.Sosat(), "Сосать");
//            Assert.AreEqual(test_res5.Sosat(), "Сосать");
//            Assert.AreEqual(test_res6.Print(), "Сосать");
//            Assert.AreEqual(test_res7.Print(), "Не Сосать");
//        }
//
//        [Test]
//        public void DataSetTest()
//        {
//            var readStorage = new ReadStorage();
//            var writeStorage = new WriteStorage();
//
//            var readStorageWrapper = new StorageWrapper<ReadStorage>(readStorage, new UserService("name"));
//            var writeStorageWrapper =
//                new StorageWrapper<WriteStorage>(writeStorage, new UserService("name"));
//            var dataset = new DataSet<ReadStorage, WriteStorage>(readStorageWrapper, writeStorageWrapper);
//            var res = dataset.ReadStorageCollection<Dog>();
//            var dog = dataset.ReadStorageSet<Dog>();
//            Assert.AreEqual(10, dataset.ReadStorageCollection<Dog>().Count());
//        }
//
//        [Test]
//        public void Test123()
//        {
//            var dataSet = new DataSet(new InfoStorageWrapper(new InfoStorage(), new UserService("name")),
//                new ActionStorageWrapper(new UserActionInfoStorage(), new UserService("name")));
//            var coll = dataSet._actionService.SetCollection<string>();
//            coll[1] = "";
//            var coll1 = dataSet._actionService.SetCollection<string>();
//            Assert.AreEqual("", coll1[1]);
//        }
//
//        private string s = null;
//        public string getNull()
//        {
//            return s;
//        }
//        [Test]
//        public void NullTest()
//        {
//            var ss = getNull();
//            s = "21";
//            Assert.AreEqual("21",getNull());
//        }
//    }
//
//    #region creatservice
//
//    public class TestService4
//    {
//        private readonly TestService1 S1;
//        private readonly TestService2 S2;
//        private readonly TestService3 S3;
//        private readonly TestService S;
//
//        public TestService4(TestService s, TestService1 s1, TestService2 s2, TestService3 s3)
//        {
//            S1 = s1;
//            S2 = s2;
//            S3 = s3;
//            S = s;
//        }
//
//        public string Sosat()
//        {
//            return S3.Sosat();
//        }
//    }
//
//
//    public class TestService5
//    {
//        private readonly TestService1 S1;
//        private readonly TestService2 S2;
//        private readonly TestService3 S3;
//        private readonly TestService4 S4;
//        private readonly TestService S;
//
//        public TestService5(TestService s, TestService1 s1, TestService2 s2, TestService3 s3, TestService4 s4)
//        {
//            S1 = s1;
//            S2 = s2;
//            S3 = s3;
//            S4 = s4;
//            S = s;
//        }
//
//        public string Sosat()
//        {
//            return S3.Sosat();
//        }
//    }
//
//    public class TestService6
//    {
//        private readonly TestService1 S1;
//        private readonly TestService2 S2;
//        private readonly TestService3 S3;
//        private readonly TestService4 S4;
//        private readonly TestService5 S5;
//        private readonly TestService S;
//
//        public TestService6(TestService s, TestService1 s1, TestService2 s2, TestService3 s3, TestService4 s4,
//            TestService5 s5)
//        {
//            S1 = s1;
//            S2 = s2;
//            S3 = s3;
//            S4 = s4;
//            S5 = s5;
//            S = s;
//        }
//
//        public string Sosat()
//        {
//            return S3.Sosat();
//        }
//    }
//
//
//    public class TestService7
//    {
//        private readonly TestService1 S1;
//        private readonly TestService2 S2;
//        private readonly TestService3 S3;
//        private readonly TestService4 S4;
//        private readonly TestService5 S5;
//        private readonly TestService6 S6;
//        private readonly TestService S;
//
//        public TestService7(TestService s, TestService1 s1, TestService2 s2, TestService3 s3, TestService4 s4,
//            TestService5 s5, TestService6 s6)
//        {
//            S1 = s1;
//            S2 = s2;
//            S3 = s3;
//            S4 = s4;
//            S5 = s5;
//            S6 = s6;
//            S = s;
//        }
//
//        public string Sosat()
//        {
//            return S3.Sosat();
//        }
//    }
//
//    #endregion
//
//
//    public class Dog
//    {
//        public string Result()
//        {
//            return "123";
//        }
//
//        public int Age { get; set; }
//        public Dog dog { get; set; }
//    }
//
//    public partial class ReadStorage : Storage
//    {
//        public IEnumerable<Dog> Dogs => Enumerable.Range(1, 100).Select(x => new Dog {Age = x});
//
//        public Dog Dog { get; set; } = new Dog
//        {
//            Age = 21, dog = new Dog {Age = 12}
//        };
//    }
//
//    public partial class WriteStorage : Storage
//    {
//    }
//
//    public class DogsPermissionFilter : PermissionFilter<Dog>
//    {
//        private readonly UserService Service;
//
//        public DogsPermissionFilter(UserService service) : base(service)
//        {
//            Service = service;
//        }
//
//        public override IEnumerable<Dog> Filter(IEnumerable<Dog> infoStorage)
//        {
//            return Service.Username == "name" ? infoStorage.Take(10) : infoStorage;
//        }
//    }
//
//    public class WriteStorageWrapper : StorageWrapper<WriteStorage>
//    {
//        public WriteStorageWrapper(WriteStorage storage, IUserService userService) : base(storage, userService)
//        {
//        }
//    }
//}